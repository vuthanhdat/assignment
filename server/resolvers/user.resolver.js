import postModel from "../models/post";

export default {
    addUser: (root, args, context, info) => {
        let user = new postModel({
            username: args.username,
            password: args.password,
            time: Date()
        });
        return user.save((err) => {
            if (err) {
                console.log(err);
                return new Error("Cannot add entity to collection");
            }
        });
    },
    deleteUser: () => {

    }
};

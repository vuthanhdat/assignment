import UserModel from '../models/user';
import PostModel from '../models/post';

export default {
  Query: {
    users: () => UserModel.find({}),
    posts: () => PostModel.find({}),
  },
  Mutation: {
    addPost: (root, args, context, info) => {
      console.log(context, info);
      const post = new PostModel({
        content: args.content,
        username: args.username,
        time: Date(),
      });
      return post.save();
    },
  },
};

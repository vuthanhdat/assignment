import { gql } from 'apollo-server-express';

export default gql`
    scalar Date

    type Post {
        id: ID!
        content: String!
        username: String!
        time: Date!
    }

    type User {
        id: ID!
        name: String!
        username: String!
    }

    #Query
    type Query {
        posts: [Post!]!
        post(id: ID!): Post
        postsByUser(username: String!): [Post!]!
        users: [User!]!
        user(id: ID!): User
    }

    #Mutation
    type Mutation {
        deleteUser(id: ID!): Int!
        addUser(name: String!, username: String!): User
        updateUser(name: String!, username: String!): User
        addPost(content: String!, username: String!): Post
        deletePost(id: ID!): Int!
        updatePost(content: String!, username: String!, time: String!): Post
    }
`;

import express from 'express';
import { ApolloServer } from 'apollo-server-express';

import { connect, connection } from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';
import typeDefs from './schema/schema';
import resolvers from './resolvers';

// auto find and map .env file
dotenv.config();

// connect to local database
connect(process.env.MONGODB_URI, { useNewUrlParser: true }).catch((e) => {
  console.log('Cannot connect to DB');
  throw e;
});
connection.once('open', () => {
  console.log(`connected to mongodb with DB URI: ${process.env.MONGODB_URI}`);
});

const app = express();

const server = new ApolloServer(
  {
    typeDefs,
    resolvers,
    introspection: true,
    playground: {
      settings: {
        'editor.theme': 'light',
      },
    },
  }
);

server.applyMiddleware({ app });

app.use(cors());

app.listen(process.env.PORT, () => {
  console.log(`Express Apollo Server is Running with graphql path: localhost:${process.env.PORT}${server.graphqlPath}`);
});

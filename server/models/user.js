import { model, Schema } from 'mongoose';

const userSchema = new Schema({
    name: String,
    username: String,
    password: String
});

export default model('User', userSchema);



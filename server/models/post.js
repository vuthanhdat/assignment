import { model, Schema} from 'mongoose';

const postSchema = new Schema({
    content: String,
    username: String,
    time: Date
});

export default model('Post', postSchema);

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const ROOT = document.getElementById('root');
ReactDOM.render(
    <div className="App">
        <App />
    </div>
, ROOT);

export const _isEmpty = (content) => {
    return !content || !content.trim();
};

export const splitWordsPreserve = (content, len = 50) => {
    if(_isEmpty(content)){
        return [];
    }
    content = processSpaces(content);
    if(content.length <= len){
        return [content];
    }
    let splitCount = Math.ceil(content.length / len);
    let result = [];

    let toPos = len;
    let i = 0;
    for(i=0;i<splitCount;i++){
        content = setContentPage(i+1 ,splitCount, content);
        if(content.length <= 50){
            result[i] = content;
            break;
        }
        while (_isWordsAtIndex(content, toPos)){
            toPos = toPos -1;
        }
        result[i] = shorten(content, 0, toPos);
        content = shorten(content, toPos, content.length -1);
        toPos = len;
    }
    return result;
};

export const setContentPage = (currentPage, totalPage, content) => {
    return `${currentPage}/${totalPage} ${content}`;
};

export const _isWordsAtIndex = (content, pos) => {
    return content.charAt(pos) !== ' ';
};

export const shorten = (str, fromPos, toPos) => {
    return str.substr(fromPos, toPos).trim();
};

export const processSpaces = (content) => {
    if(_isEmpty(content)){
        return '';
    }
    content = content.trim().replace(/[\r\n]+/g," ").replace(/\s+/g, " ");
    return content;
};

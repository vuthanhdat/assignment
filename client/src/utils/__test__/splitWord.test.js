/**
 * Test injectors
 */

import {processSpaces, splitWordsPreserve} from '../splitWord';

describe('splitWord', () => {

    let content;
    let contentWithSpaces;
    let element1;
    let element2;

    beforeAll(() => {
        content = 'I can\'t believe Tweeter now supports chunking my messages, so I don\'t have to do it myself.';
        contentWithSpaces = "I can't believe Tweeter now supports      \r\r\r\r\r\r      \n\n\n       chunking                    my messages, so I don't have to do it myself.";
        element1 = "1/2 I can't believe Tweeter now supports chunking";
        element2 = "2/2 my messages, so I don't have to do it myself.";
    });

    it('should equal after process spaces', () => {
        expect(processSpaces(contentWithSpaces)).toEqual(content);
    });

    it('shoud equal length = 2', () => {
        const result = splitWordsPreserve(content, 50);
        expect(result).toHaveLength(2);
    });

    it('should return empty array', () => {
        expect(splitWordsPreserve("                                                                                                                                                                                                                                                       ")).toHaveLength(0);
        expect(splitWordsPreserve("")).toHaveLength(0);
    });

    it('50 char or less', () => {
        expect(splitWordsPreserve("original post")).toContainEqual("original post");
        expect(splitWordsPreserve("original post 1")).toContainEqual("original post 1");
        expect(splitWordsPreserve(element1)).toContainEqual(element1);
    });

    it('shoud contain element 1 & 2', () => {
        const result = splitWordsPreserve(content, 50);
        expect(result).toContainEqual(element1);
        expect(result).toContainEqual(element2);
    });
});

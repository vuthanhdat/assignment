import { gql } from 'apollo-boost';


const getPostsQuery = gql`
    {
        posts{
            id
            content
            username
            time
        }
    }`;

const addPostMutation = gql`
    mutation ($content: String!, $username: String!){
        addPost(content: $content, username: $username){
            content
            username
        }
    }
`;

export { addPostMutation, getPostsQuery };

import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import "antd/dist/antd.css";

// components
import Tweet from "./components/Tweet";

// apollo client setup
const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql'
});

class App extends Component {
  render() {
    return (
        <ApolloProvider client={client}>
            <Tweet/>
        </ApolloProvider>
    );
  }
}

export default App;

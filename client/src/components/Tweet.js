import React from 'react';
import {
    Comment, Avatar, Form, Button, List, Input,
} from 'antd';
import { graphql, compose } from 'react-apollo';
import { getPostsQuery, addPostMutation } from '../queries/queries'
import { splitWordsPreserve } from '../utils/splitWord';

const TextArea = Input.TextArea;

const CommentList = ({ comments }) => (
    <List
        dataSource={comments}
        header={`${comments.length} ${comments.length > 1 ? 'posts' : 'post'}`}
        itemLayout="horizontal"
        renderItem={props => <Comment {...props} />}
    />
);

const Editor = ({
                    onChange, onSubmit, submitting, value, validateStatus
                }) => (
    <div>
        <Form.Item>
            <TextArea validateStatus={validateStatus} rows={4} onChange={onChange} value={value} />
        </Form.Item>
        <Form.Item>
            <Button
                htmlType="submit"
                loading={submitting}
                onClick={onSubmit}
                type="primary"
            >
                Post
            </Button>
        </Form.Item>
    </div>
);

class Tweet extends React.Component {
    state = {
        validateStatus: 'validating',
        validateMsg: '',
        submitting: false,
        value: '',
    };

    setComment = (posts) => {
       if(!posts) return [];
       return posts.map(post => {
           return {
               author: post.username,
               avatar: <Avatar
                   icon="user"
                   alt="Avatar"
               />
               ,
               content: <p>{post.content}</p>,
               datetime: post.time,
           }
       })
    };

    handleSubmit = () => {
        const { value } = this.state;
        if (!value || this.validateWord(value)) {
            return;
        }

        const processedContent = splitWordsPreserve(value, 50);

        this.setState({
            submitting: true,
        });

        setTimeout(() => {
            processedContent.forEach(item => {
                this.props.addPostMutation({
                    variables: {
                        content: item,
                        username: "Dat",
                    },
                    refetchQueries: [{ query: getPostsQuery }]
                });
            });
            this.setState({
                submitting: false,
                value: ''
            });
        }, 1000);
    };

    validateWord = (content) => {
        if(content && content.length > 50){
            if(content.substr(0, 50).indexOf(" ") === -1){
                return true;
            }
        }
        return false;
    };

    handleChange = (e) => {
        if(this.validateWord(e.target.value)){
            this.setState({
                validateStatus: 'error',
                validateMsg: 'In 50 character must have 1 spaces or more',
            });
        }
        this.setState({
            value: e.target.value,
            validateStatus: 'validating',
            validateMsg: '',
        });
    };

    render() {
        const data = this.props.getPostsQuery;
        const { submitting, value, validateStatus, validateMsg } = this.state;
        const posts = data && data.posts ? data.posts : [];
        const comments = this.setComment(posts);
        return (
            <div>
                {comments.length > 0 && <CommentList comments={comments} />}
                <Comment
                    avatar={(
                        <Avatar
                            icon="user"
                            alt="Avatar"
                        />
                    )}
                    content={(
                        <Editor
                            validateStatus={validateStatus}
                            onChange={this.handleChange}
                            onSubmit={this.handleSubmit}
                            submitting={submitting}
                            value={value}
                        />
                    )}
                />
                { validateMsg ? validateMsg : '' }
            </div>
        );
    }
}

export default compose(
    graphql(getPostsQuery, {name: "getPostsQuery"}),
    graphql(addPostMutation, {name: "addPostMutation"})
)(Tweet);

# Project TwitSplit.

# Installation.

TwitSplit requires:

* [Node.js](https://nodejs.org/)
* [MongoDB](https://www.mongodb.com/)

# Usage.
Open the command line and switch the location to project directory```client``` and ```server```, run following commands to build KAC system:

- First of all you need to install dependencies to start project `client` and `server`:

```sh
> cd client 
> npm install
> npm run start
```
```sh
> cd server
> npm install
> npm run start
```



# Unit Testing TwitSplit

```sh
> cd client
> npm run test
```


# Change DB Connection to your local

```sh
access to server/.env file and change MONGODB_URI
MONGODB_URI=<your connection string>
```
